#!/usr/bin/env python
# coding: utf-8

from collections import namedtuple

Red = namedtuple('Red', ('src', 'dst'))
Blue = namedtuple('Blue', ('src', 'dst'))
